package me.dm7.barcodescanner.zbar.sample;

import android.content.Intent;
import android.hardware.Camera;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.widget.Toast;

import me.dm7.barcodescanner.zbar.Result;
import me.dm7.barcodescanner.zbar.ZBarScannerView;

public class SimpleScannerActivity extends ActionBarActivity implements ZBarScannerView.ResultHandler {
    private ZBarScannerView mScannerView;

    @Override
    public void onCreate(Bundle state) {
        super.onCreate(state);
        mScannerView = new ZBarScannerView(this);
        setContentView(mScannerView);

        mScannerView.setAutoFocus(true);
    }

    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this);

        if ("POYNT".equals(Build.MANUFACTURER)) {
            mScannerView.setCameraPreviewRotation(90);
        }
        mScannerView.startCamera();
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();
    }

    @Override
    public void handleResult(Result rawResult) {

        String ResultContents = rawResult.getContents();
        String Fornat = rawResult.getBarcodeFormat().getName();

        mScannerView.startCamera();

        Intent intent = new Intent(this, ResultActivity.class);
        intent.setType("text/html");

        Bundle b = new Bundle();
        b.putString("content", ResultContents);
        b.putString("format", Fornat);
        intent.putExtras(b);

        startActivity(intent);
        Toast.makeText(this, "Contents = " + rawResult.getContents() +
                ", Format = " + rawResult.getBarcodeFormat().getName(), Toast.LENGTH_LONG).show();

    }
}
