package me.dm7.barcodescanner.zbar.sample;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class ResultActivity extends Activity {

    TextView resultTextView, formatTextView;
    Button goBackBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_result);

        resultTextView = (TextView) findViewById(R.id.result);
        formatTextView = (TextView) findViewById(R.id.format);
        goBackBtn = (Button) findViewById(R.id.goBackButton);

        Bundle bundle = getIntent().getExtras();
        String pResultContent = bundle.getString("content");
        String pFormat = bundle.getString("format");

        resultTextView.setText(pResultContent);
        formatTextView.setText(pFormat);


    }

    public void onGoBackClicked(View v) {
        Intent backIntent = new Intent(this, SimpleScannerActivity.class);
        startActivity(backIntent);
    }
}
